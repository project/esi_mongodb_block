
-- SUMMARY --
This module is a plugin for the esi_api module which provides integration with 
mongodb_block module.

For a full description of the module, please visit the project page:
  http://drupal.org/project/esi_mongodb_block

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/esi_mongodb_block


-- REQUIREMENTS --

Requires esi_api module and mongodb_block module.


-- INSTALLATION --

* Install this module into the esi_api/modules directory.
* Enable the module.


-- CONFIGURATION --
* Configure settings at admin/config/development/esi/mongodb_block
