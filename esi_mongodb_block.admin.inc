<?php

/**
 * @file
 * ESI MongoDB Block administration pages.
 */

/**
 * Common ESI configuration.
 */
function esi_mongodb_block_admin_settings($form, &$form_state) {
  $form['esi_mongodb_block_caching_policy'] = array(
    '#type' => 'radios',
    '#title' => t('Caching policy'),
    '#options' => array(
      NULL => t('All goes ESI'),
      DRUPAL_CACHE_GLOBAL => t('All except globally cached blocks'),
      DRUPAL_CACHE_PER_PAGE => t('All except per page and globally cached blocks'),
      DRUPAL_CACHE_CUSTOM => t('Only selected blocks'),
    ),
    '#default_value' => variable_get('esi_mongodb_block_caching_policy', DRUPAL_CACHE_PER_PAGE),
    '#description' => t('Selecting the first or second option will allow some blocks to be cached with the page rather than served through ESI. This can be useful in order to achieve aggressive caching efficiently.'),
  );

  return system_settings_form($form);
}
